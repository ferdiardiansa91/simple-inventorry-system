
<h3>Laporan Pembayaran</h3>
<table>
	<tbody>
	<tr>
		<th style="width :1cm;">No</th>
		<th style="width :100cm;">Nama</th>
		<th>Nominal</th>
		<th>Debit</th>
		<th>Kredit</th>
	</tr>
		<?php 
			$nomor 			= 1;
			$total_kredit	= array();
			$total_debit	= array();
			foreach ($data as $kolom) {
		?>
			<tr>
				<td><?php echo $nomor++ ?></td>
				<td><?php echo $kolom->nama ?></td>
				<td><?php echo number_format($kolom->nominal, 0, ",", ".") ?></td>
				<td><?php echo number_format($kolom->debit, 0, ",",".") ?></td>
				<td><?php echo number_format($kolom->kredit, 0, ",",".") ?></td>
			</tr>
		<?php
				$total_kredit[] 	= $kolom->kredit;		
				$total_debit[] 		= $kolom->debit;		
			}
		 ?>
		 <tr>
		 	
			<th colspan="3" style="text-align: right">Total (Rp)</th>
			<th><?php echo number_format(array_sum($total_debit), 0, ",",".") ?></th>
			<th><?php echo number_format(array_sum($total_kredit), 0, ",",".") ?></th>
		 </tr>
	</tbody>
</table>



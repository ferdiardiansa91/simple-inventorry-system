$(document).ready(function() {
        $('#bt_tambah').on('click', function() {
            $('#modal-tambah').modal('show');
        });

        $('#modal-tambah, #modal-edit').on('shown.bs.modal', function() {
            $('#kd_jenis, #edit_kd_jenis').focus();
        });

        $('#modal-tambah, #modal-edit').on('hidden.bs.modal', function() {
            $('#form-tambah').validate().resetForm();
            $('#form-edit').validate().resetForm();
            $('#form-tambah, #form-edit').find('input[type="text"]').val('');
            $('#form-tambah, #form-edit').find('.form-group').removeClass('has-error');
        });
    });

        /*PROSES TAMBAH DATA*/
        $('#form-tambah').validate({
            rules : {
                kd_jenis : {
                    required : true
                },
                jenis : {
                    required : true
                }
            },
            messages : {
                kd_jenis : {
                    required : 'kolom belum diisi'
                },
                jenis : {
                    required : 'kolom belum diisi'
                }
            },
            highlight : function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight : function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            submitHandler : function() {
                // Save ke database
                var kd_jenis    = $('#kd_jenis').val();
                var jenis       = $('#jenis').val();
                var jenis       = $('#jenis').val();
                var table       = $('#tabel-jenis').DataTable();

                $.ajax({
                    url     : 'http://localhost/portofolio/softdroid/index.php/jenis/insert',
                    data    : 'kd_jenis='+kd_jenis+'&jenis='+jenis+'&jenis='+jenis,
                    type    : "POST",
                    dataType: 'json',
                    success : function(data) {

                        if(data == 1) {
                            $('#loader').fadeIn('slow');
                            $('#modal-tambah').modal('hide');
                            $('#kd_jenis').val('');
                            $('#jenis').val('');
                            $('#jenis').val('kosong');


                            window.setTimeout(function() {
                                $('#alert-success').fadeOut('fast');
                                $('#alert-change').fadeOut('fast');
                                $('#loader').fadeOut('slow');
                                $('#alert-success').fadeIn('fast');
                                table.ajax.reload();
                            }, 2000);
                        }
                    }
                })
            }/*end submit handler*/
        });

    /*_______________DATATABLE__________________*/

    var table = $('#tabel-jenis').DataTable({
        "displayLength"    : 5,
        "processing"    : true,
        "serverside"    : true,
        "ajax"          : {
                            "url"    :  "http://localhost/portofolio/softdroid/index.php/jenis/get_data"
                        },
            "columns"       : [
                            {"data" : "kd_jenis"},
                            {"data" : "jenis"},
                            {"defaultContent" : "<center><div class='btn-group'>"+
                                    "<button type='button' class='btn btn-info dropdown-toggle btn-xs' data-toggle='dropdown' aria-expanded='false'>Pilih aksi  <span class='caret'></span></button>"+
                                    "<ul class='dropdown-menu dropdown-menu-right' role='menu' style='font-size: 11px;'>"+
                                        "<li><a id='edit' href='javascript:void(0)'><i class='fa fa-pencil'></i> Ubah</a></li>"+
                                        "<li><a id='hapus' onclick='' href='javascript:void(0)'><i class='fa fa-trash'></i> Hapus</a></li>"+
                                    "</ul>"+
                                "</div></center>" },

            ]
    });

    // Show modal edit 
    $('#tabel-jenis tbody').on('click','#edit', function() {

        var data = table.row( $(this).parents('tr') ).data();

        $('#modal-edit').modal('show');
        $('#edit_kd_jenis').val(data.kd_jenis);
        $('#edit_jenis').val(data.jenis);
        $('#id').val(data.id);

    });

    // Proces to update data
    $('#form-edit').validate({
        rules : {
            kd_jenis : {
                required : true
            },
            jenis : {
                required : true
            }
        },
        messages : {
            kd_jenis : {
                required : 'kolom belum diiisi'
            },
            jenis : {
                required : 'kolom belum diiisi'
            }
        },
        highlight : function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight : function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        submitHandler : function() {

           var id           =  $('#id').val();
           var kd_jenis     =  $('#edit_kd_jenis').val();
           var jenis        =  $('#edit_jenis').val();

           $.ajax({
                url     : 'http://localhost/portofolio/softdroid/index.php/jenis/edit',
                data    : 'id='+id+'&kd_jenis='+kd_jenis+'&jenis='+jenis+'&jenis='+jenis,
                type    : 'POST',
                dataType: 'json',
                success : function(data) {

                    if(data == 1) {
                        $('#loader').fadeIn('slow');
                        $('#modal-edit').modal('hide');
                        // reload table
                        window.setTimeout(function() {
                            $('#alert-success').fadeOut('fast');
                            $('#alert-delete').fadeOut('fast');
                            $('#loader').fadeOut('slow');
                            $('#alert-change').fadeIn('fast');
                            table.ajax.reload();
                        }, 2000);
                    }
                }
           });
        }
    });

    // Hapus data
    $('#tabel-jenis tbody').on('click','#hapus', function() {

        var data = table.row( $(this).parents('tr') ).data();
        $('#modal-hapus').modal('show');

        $('#bt_delete').on('click', function() {

            $.ajax({
                url     : 'http://localhost/portofolio/softdroid/index.php/jenis/delete',
                data    : 'kd_jenis='+data.kd_jenis,
                type    : 'POST',
                dataType: 'json',
                success : function(data) {

                    if(data == 1) {
                        $('#loader').fadeIn('slow');
                        $('#modal-hapus').modal('hide');
                        // reload table
                        window.setTimeout(function() {
                            $('#alert-success').fadeOut('fast');
                            $('#alert-change').fadeOut('fast');
                            $('#loader').fadeOut('slow');
                            $('#alert-delete').fadeIn('fast');
                            table.ajax.reload();
                        }, 2000);
                    }
                }
            });
        });
    });
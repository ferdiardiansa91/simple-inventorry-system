<div class="row">
    <div class="col-sm-8">
        <div class="card">
            <div class="alert alert-success fresh-color" id="alert-success" role="alert" hidden="true">
                <strong>Well done !</strong> Data successfully save 
            </div>

            <div class="alert alert-warning fresh-color" id="alert-change" role="alert" hidden="true">
                <strong>Well done !</strong> Data successfully change 
            </div>

            <div class="alert alert-danger fresh-color" id="alert-delete" role="alert" hidden="true">
                <strong>Well done !</strong> Data successfully remove 
            </div>

            <div class="card-header">
                <div class="card-title">
                    <div class="title"> Data Barang </div>
                </div>

                <div class="pull-right">
                    <div class="col-sm-6">
                        <button type="button" id="bt_tambah" class="btn btn-primary btn-sm"> <i class="fa fa-plus"></i> Buat baru</button>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <table id="tabel-barang" class="table table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="20">Barcode</th>
                            <th></th>
                            <th>Kode</th>
                            <th>Nama Barang</th>
                            <th>Jenis</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Kode</th>
                            <th>Position</th>
                            <th>Jenis</th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div><!-- end card-body -->
        </div><!-- end card -->
    </div><!-- end col-xs-6 -->
</div><!-- end row -->


<!-- MODAL TAMBAH -->
<div class="modal fade modal-defualt" id="modal-tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Buat baru</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-8">
                        <form class="form-horizontal" id="form-tambah">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Kode</label>
                                <div class="col-sm-7">
                                    <input type="text" name="kd_barang" id="kd_barang" class="form-control input-sm" required autofocus></input>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-7">
                                    <input type="text" name="nm_barang" id="nm_barang" class="form-control input-sm"  required></input>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Jenis</label>
                                <div class="col-sm-5">
                                    <select name="jenis" id="jenis" class="form-control" style="font-size: 11px;" required>
                                        <option value=""> ... </option>
                                        <?php 
                                            foreach ($jenis as $kolom) {
                                                echo "<option value='$kolom->kd_jenis'>$kolom->jenis</option>";
                                            }

                                         ?>
                                    </select>
                                </div>
                            </div>
                    </div><!-- akhir div col-sm-6 -->
                </div><!-- akhir row -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">     
                <i class="fa fa-remove"></i> Batal
                </button>
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-check"></i> Simpan
                </button>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- MODAL EDIT -->
<div class="modal fade modal-default" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ubah Data</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-8">
                        <form class="form-horizontal" id="form-edit">
                            <input type="hidden" name="id" id="id">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Kode</label>
                                <div class="col-sm-7">
                                    <input type="text" name="kd_barang" id="edit_kd_barang" class="form-control input-sm" required autofocus></input>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-7">
                                    <input type="text" name="nm_barang" id="edit_nm_barang" class="form-control input-sm"  required></input>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Jenis</label>
                                <div class="col-sm-5">
                                    <select name="jenis" id="edit_jenis" class="form-control" style="font-size: 11px">
                                        <option value=""> ... </option>
                                        <?php 
                                            foreach ($jenis as $kolom) {
                                                echo "<option value='$kolom->kd_jenis'>$kolom->jenis</option>";
                                            }

                                         ?>
                                    </select>
                                </div>
                            </div>
                    </div><!-- akhir div col-sm-6 -->
                </div><!-- akhir row -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">     
                <i class="fa fa-remove"></i> Batal
                </button>
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-check"></i> Simpan
                </button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- MODAL HAPUS -->
<div class="modal fade modal-default" id="modal-hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Apakah anda yakin ingin menghapus data ini ?</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">     
                <i class="fa fa-remove"></i> Tidak
                </button>
                <button type="button" id="bt_delete" class="btn btn-primary">
                    <i class="fa fa-check"></i> Ya
                </button>
            </div>
        </div>
    </div>    
</div>

<div class="modal fade modal-default" id="modal-lihat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-4">
                            <label>Nama</label>
                        </div>

                        <div class="col-sm-8">
                            <label id="lihat-nama">Nama Barang</label>
                        </div>
                    </div>   

                    <div class="form-group">
                         <div class="col-sm-4">
                            <label>Jenis</label>
                        </div>

                        <div class="col-sm-8">
                            <label id="lihat-jenis">Jenis</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4">
                            <label>Barcode</label>
                        </div>

                        <div class="col-sm-4">
                            <img id="img-barcode" style="width: 100px;" src="">
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm">
                    <i class="fa fa-print"></i> Print
                </button>
                </form>
            </div> -->
        </div>
    </div>
</div>


<style type="text/css">
    .btn-group{
        font-size: 10px !important;
    }
</style>
<script type="text/javascript" src="<?php echo base_url() ?>/assets/js/JsBarcode-master/dist/JsBarcode.all.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/app/js/barang.js"></script>

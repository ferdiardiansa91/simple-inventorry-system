<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('model_barang');
		
		// Load barcode image
		include(APPPATH.'third_party/picqer/php-barcode-generator/src/BarcodeGenerator.php');
		include(APPPATH.'third_party/picqer/php-barcode-generator/src/BarcodeGeneratorPNG.php');
		include(APPPATH.'third_party/picqer/php-barcode-generator/src/BarcodeGeneratorSVG.php');
		include(APPPATH.'third_party/picqer/php-barcode-generator/src/BarcodeGeneratorHTML.php');
	}

	public function index()
	{

		$data['generatorPNG']  = new Picqer\Barcode\BarcodeGeneratorPNG();
		$data['generatorSVG']  = new Picqer\Barcode\BarcodeGeneratorSVG();
		$data['generatorHTML'] = new Picqer\Barcode\BarcodeGeneratorHTML();

		$data['konten']		= 'halaman/barang/index';

		$data['jenis'] 	= $this->db->distinct('kd_jenis')->get('tb_jenis')->result();
		
		$this->load->view('home', $data);
	}

	public function get_data() {
		$this->db->select('tb_barang.*, tb_jenis.kd_jenis,tb_jenis.jenis');
		$this->db->join('tb_jenis', 'tb_jenis.kd_jenis = tb_barang.kd_jenis');
		$data['data'] 	= $this->db->get('tb_barang')->result();


		echo json_encode($data);
	}

	public function insert() {
		$data['kd_barang']		= $this->input->post('kd_barang');
		$data['nm_barang']		= $this->input->post('nm_barang');
		$data['kd_jenis']		= $this->input->post('jenis');

		// print_r($data);
		$qry 	= $this->model_barang->get_insert($data);

		if($qry) {
			echo json_encode(1);
		}
	}

	public function edit() {
		$data['id']				= $this->input->post('id');
		$data['kd_barang']		= $this->input->post('kd_barang');
		$data['nm_barang']		= $this->input->post('nm_barang');
		$data['kd_jenis']		= $this->input->post('jenis');

		$qry 	= $this->model_barang->get_update($data['id'],$data);

		if ($qry) {
			echo json_encode(1);
		}
	}

	public function delete() {
		$kd_barang 		= $this->input->post('kd_barang');

		$qry 	= $this->model_barang->get_delete($kd_barang);

		if($qry) {
			echo json_encode(1);
		}
	}

	public function cetak() {
		$kode 	= $this->input->get('kd_barang', TRUE);

		echo $kode;
	}
}

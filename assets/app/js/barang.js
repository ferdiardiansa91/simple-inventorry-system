$(document).ready(function() {
        $('#bt_tambah').on('click', function() {
            $('#modal-tambah').modal('show');
        });
    });

    /*PORSES MODAL TAMBAH*/
    $('#form-tambah').validate({
        rules : {
            kd_barang : {
                required : true
            },
            nm_barang : {
                required : true
            },
            jenis : {
                required : true
            }
        },
        messages : {
            kd_barang : {
                required : 'kolom belum diisi'
            },
            nm_barang : {
                required : 'kolom belum diisi'
            },
            jenis : {
                required : 'anda belum memilih jenis'
            }
        },
        highlight : function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight : function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        submitHandler : function () {
            // $('#bt_simpan').on('click', function() {
                var kd_barang   = $('#kd_barang').val();
                var nm_barang   = $('#nm_barang').val();
                var jenis       = $('#jenis').val();
                var table       = $('#tabel-barang').DataTable();

                // Save ke database
                $.ajax({
                    url     : 'http://localhost/portofolio/softdroid/index.php/barang/insert',
                    data    : 'kd_barang='+kd_barang+'&nm_barang='+nm_barang+'&jenis='+jenis,
                    type    : "POST",
                    dataType: 'json',
                    success : function(data) {
                        $('#loader').fadeIn('slow');

                        if(data == 1) {
                            $('#loader').fadeIn('slow');
                            $('#modal-tambah').modal('hide');
                            $('#kd_barang').val('');
                            $('#nm_barang').val('');
                            $('#jenis').val('kosong');

                            window.setTimeout(function() {
                                $('#alert-change').fadeOut('fast');
                                $('#alert-delete').fadeOut('fast');
                                $('#loader').fadeOut('slow');
                                $('#alert-success').fadeIn('fast');
                                $('#loader').fadeOut('slow');
                                table.ajax.reload();
                            }, 2000);
                        }
                    }
                })
            // });
        }/*end submit handler*/
    });

    /*PROSES MODAL OPEN*/
    $('#modal-tambah, #modal-edit').on('shown.bs.modal', function() {
        $('#kd_barang, #edit_kd_barang').focus();
    });
    /*PROSES MODAL CLOSE*/
    $('#modal-tambah, #modal-edit').on('hidden.bs.modal', function() {
        $('#form-tambah, #form-edit').find('.form-group').removeClass('has-error');
        $('#form-tambah').validate().resetForm();
        $('#form-edit').validate().resetForm();
        $('#form-tambah, #form-edit').find('input[type="text"],select').val('');
    });

    /*_________________DATATABLE_________________________*/

    var table = $('#tabel-barang').DataTable({
        "select"        : true,    
        "processing"    : true,
        "serverside"    : true,
        "ajax"          : {
                            "url"    :  "http://localhost/portofolio/softdroid/index.php/barang/get_data"
                        },
            "columns"       : [
                            {"defaultContent" : "<a id='lihat_barcode' href='javascript:void(0)'><img style='width:40px;margin-left : 15px;cursor:pointer;' src='http://localhost/portofolio/softdroid/img/no-image.png'></a>"},
                            {"data" : "id"},
                            {"data" : "kd_barang"},
                            {"data" : "nm_barang"},
                            {"data" : "jenis"},
                            {"defaultContent" : "<div class='btn-group'>"+
                                    "<button type='button' class='btn btn-info dropdown-toggle btn-xs' data-toggle='dropdown' aria-expanded='false'>Pilih aksi  <span class='caret'></span></button>"+
                                    "<ul class='dropdown-menu dropdown-menu-right' role='menu' style='font-size:11px'>"+
                                        "<li><a id='edit' href='javascript:void(0)'><i class='fa fa-pencil'></i> Ubah</a></li>"+
                                        "<li><a id='hapus' onclick='' href='javascript:void(0)'><i class='fa fa-trash'></i> Hapus</a></li>"+
                                    "</ul>"+
                                "</div>" }

            ],
            "order": [[ 1, "asc" ]],
            "columnDefs": [
                {
                    "targets": [ 1 ],
                    "visible": false,
                    "searchable": false
                }
            ]
        
    });

    /*SHOW BARCODE*/
    $('#tabel-barang tbody').on('click','#lihat_barcode', function() {
        var data = table.row( $(this).parents('tr') ).data();
        
        $('#modal-lihat').modal('show');
        $("#img-barcode").JsBarcode(data.kd_barang);
        $('#lihat-nama').html(data.nm_barang);
        $('#lihat-jenis').html(data.jenis);
    });

    // SHOW MODAL EDIT 
    $('#tabel-barang tbody').on('click','#edit', function() {

        var data = table.row( $(this).parents('tr') ).data();

        $('#modal-edit').modal('show');
        $('#edit_kd_barang').val(data.kd_barang);
        $('#edit_nm_barang').val(data.nm_barang);
        $('#edit_jenis').val(data.kd_jenis);
        $('#id').val(data.id);

    });

    // PROCESS UPDATE DATA
    $('#form-edit').validate({
        rules : {
            kd_barang : {
                required : true
            },
            nm_barang : {
                required : true
            },
            jenis : {
                required : true
            }
        },
        messages : {
            kd_barang : {
                required : 'kolom belum diisi'
            },
            nm_barang : {
                required : 'kolom belum diisi'
            },
            jenis : {
                required : 'anda belum memilih jenis'
            }
        },
        highlight : function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight : function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        submitHandler : function () {
           var id           =  $('#id').val();
           var kd_barang    =  $('#edit_kd_barang').val();
           var nm_barang    =  $('#edit_nm_barang').val();
           var jenis        =  $('#edit_jenis').val();

           $.ajax({
                url     : 'http://localhost/portofolio/softdroid/index.php/barang/edit',
                data    : 'id='+id+'&kd_barang='+kd_barang+'&nm_barang='+nm_barang+'&jenis='+jenis,
                type    : 'POST',
                dataType: 'json',
                success : function(data) {

                    if(data == 1) {
                        $('#loader').fadeIn('slow');

                        $('#modal-edit').modal('hide');
                        // reload table
                        window.setTimeout(function() {
                            $('#alert-success').fadeOut('fast');
                            $('#alert-delete').fadeOut('fast');
                            $('#loader').fadeOut('slow');
                            $('#alert-change').fadeIn('fast');
                            table.ajax.reload();
                        }, 2000);
                    }
                }
           });
        }
    });

    // Hapus data
    $('#tabel-barang tbody').on('click','#hapus', function() {

        var data = table.row( $(this).parents('tr') ).data();
        $('#modal-hapus').modal('show');

        $('#bt_delete').on('click', function() {

            $.ajax({
                url     : 'http://localhost/portofolio/softdroid/index.php/barang/delete',
                data    : 'kd_barang='+data.kd_barang,
                type    : 'POST',
                dataType: 'json',
                success : function(data) {

                    if(data == 1) {
                        $('#loader').fadeIn('slow');

                        $('#modal-hapus').modal('hide');
                        // reload table
                        window.setTimeout(function() {
                            $('#alert-success').fadeOut('fast');
                            $('#alert-change').fadeOut('fast');
                            $('#loader').fadeOut('slow');
                            $('#alert-delete').fadeIn('fast');
                            table.ajax.reload();
                        }, 2000);
                    }
                }
            });
        });
    });

    $('#tabel-barang tbody').on('click','#cetak', function() {
        var data = table.row( $(this).parents('tr') ).data();
        // alert('hai');
        window.open('http://localhost/portofolio/softdroid/index.php/barang/cetak?kd-barang='+data.kd_barang, '_blank','width=400;height=200;');
    });
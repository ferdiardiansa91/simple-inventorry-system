/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.6.16 : Database - softdroid
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`softdroid` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `softdroid`;

/*Table structure for table `tb_barang` */

DROP TABLE IF EXISTS `tb_barang`;

CREATE TABLE `tb_barang` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `kd_barang` char(12) NOT NULL,
  `nm_barang` varchar(200) DEFAULT NULL,
  `kd_jenis` char(12) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tb_barang` */

insert  into `tb_barang`(`id`,`kd_barang`,`nm_barang`,`kd_jenis`,`created_at`,`updated_at`) values (4,'A00002','Meja','K003','2016-09-30 13:03:51','0000-00-00 00:00:00'),(5,'A00005','Lamborghini','K002','2016-09-30 13:06:09','0000-00-00 00:00:00');

/*Table structure for table `tb_jenis` */

DROP TABLE IF EXISTS `tb_jenis`;

CREATE TABLE `tb_jenis` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `kd_jenis` char(12) NOT NULL,
  `jenis` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tb_jenis` */

insert  into `tb_jenis`(`id`,`kd_jenis`,`jenis`) values (2,'K002','Hobi'),(3,'K003','Aksesoris');

/*Table structure for table `tb_laporan` */

DROP TABLE IF EXISTS `tb_laporan`;

CREATE TABLE `tb_laporan` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `nominal` int(12) DEFAULT NULL,
  `debit` int(11) DEFAULT NULL,
  `kredit` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `tb_laporan` */

insert  into `tb_laporan`(`id`,`nama`,`nominal`,`debit`,`kredit`) values (6,'Toyota',10000000,10000000,0),(7,'Honda',2500000,0,2500000),(8,'Mazda',85200000,0,85200000),(9,'Honda Brio',90000000,90000000,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
